import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatButtonModule } from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';
import { AccueilComponent } from './accueil/accueil.component';
import { AddPostComponent } from './add-post/add-post.component';
import { HeaderComponent } from './header/header.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { ViewPostsComponent } from './view-posts/view-posts.component';
import { FlexComponent } from './flex/flex.component';
import { ListComponent } from './list/list.component';

import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { PostsService } from './services/posts.service';


const routes : Routes = [
  {path : '', redirectTo: 'accueil', pathMatch: 'full'},
  {path : 'accueil', component: AccueilComponent},
  {path : 'posts', canActivate: [AuthGuardService] ,component: PostsComponent},
  {path : 'flex' ,component: FlexComponent},
  {path : 'list' ,component: ListComponent},
  {path : 'addpost', canActivate: [AuthGuardService] ,component: AddPostComponent},
  {path : 'not-found', component: FourOhFourComponent},
  {path: '**', redirectTo : 'not-found'}
]

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    AccueilComponent,
    AddPostComponent,
    HeaderComponent,
    FourOhFourComponent,
    ViewPostsComponent,
    FlexComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormlyModule.forRoot(),
    FormlyMaterialModule,
    MatInputModule,
    MatButtonModule,
    RouterModule.forRoot(routes),
    FlexLayoutModule,
    MatTableModule
  ],
  providers: [
    AuthGuardService,
    AuthService,
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
