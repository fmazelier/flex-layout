import { Component, OnInit, Input } from '@angular/core';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-view-posts',
  templateUrl: './view-posts.component.html',
  styleUrls: ['./view-posts.component.scss']
})
export class ViewPostsComponent implements OnInit {
  @Input() titre : string;
  @Input() contenu : string;
  @Input() date : string;
  @Input() likes : number;
  @Input() index : number;

  constructor(private postsService: PostsService) { }

  onLike(i: number) {
    this.postsService.like(i);
  }
  onDislike(i: number) {
    this.postsService.dislike(i);
  }

  ngOnInit() {
  }

}
