import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuth: boolean = false;

  constructor(private router : Router) { }

  signIn() {
    this.isAuth = true;
    setTimeout(() => {
      this.router.navigate(['posts']);
    }, 1500);
  }
  signOut() {
    this.isAuth = false;
  }
}
