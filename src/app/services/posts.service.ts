import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  posts : Array<any> = [
    {
      titre: 'premier post',
      contenu: 'ceci est le contenu de mon premier post',
      likes: 0,
      date : new Date()
    },
    {
      titre: 'deuxième post',
      contenu: 'ceci est le contenu de mon deuxième post',
      likes: 0,
      date : new Date()
    },
    {
      titre: 'troisième post',
      contenu: 'ceci est le contenu de mon troisième post',
      likes: 0,
      date : new Date()
    }
  ]

  like(i: number) {
    this.posts[i].likes ++;
  }
  dislike(i: number) {
    this.posts[i].likes --;
  }

  constructor() { }
}
