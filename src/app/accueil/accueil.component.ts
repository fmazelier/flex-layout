import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  auth: boolean;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.auth = this.authService.isAuth;
  }

  onSignIn() {
    this.authService.signIn();
  }
  onSignOut() {
    this.authService.signOut();
    this.auth = this.authService.isAuth;
  }
}
