import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';

export interface Pokemons {
  num: number;
  name: string;
  type: string;
}

const ELEMENT_DATA: Pokemons[] = [
  { num : 1, name: 'Bulbizarre', type: 'Plante' },
  { num : 2, name: 'Carapuce', type: 'Eau' },
  { num : 3, name: 'Salamèche', type: 'Feu' },
  { num : 4, name: 'Chenipan', type: 'Insecte' },
  { num : 5, name: 'Roucool', type: 'Vol' },
  { num : 6, name: 'Ratata', type: 'Normal' },
  { num : 7, name: 'Abo', type: 'Poison' },
  { num : 8, name: 'Pikachu', type: 'Electrique' },
  { num : 9, name: 'Nidoking', type: 'Poison' },
  { num : 10, name: 'Alakazam', type: 'Psy' }
];

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  displayedColumns: string[] = ['num', 'name', 'type'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
