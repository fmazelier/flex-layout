import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { Router } from '@angular/router';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {
  constructor(private router : Router, private postsService: PostsService ) { }

  ngOnInit() {
  }

  message : string;
  newPost = {
    titre : '',
    contenu : '',
    likes : 0,
    date : new Date()
  }

  form = new FormGroup({});
  model: any = {};
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [
    {
      key: 'titre',
      type: 'input',
      templateOptions: {
        label: 'Titre de votre post',
        placeholder: 'tapez votre titre ici',
        required: true,
        appearance: 'outline',
        minLength: 7,
        maxLength: 50
      },
    },
    {
      key: 'contenu',
      type: 'textarea',
      templateOptions: {
        label: 'Contenu de votre post',
        placeholder: 'tapez le contenu de votre post ici',
        required: true,
        appearance: 'outline',
        minLength: 15,
        maxLength: 250,
        rows: 5
      },
    }
  ];

  submit(model) {
    this.newPost.titre = model.titre;
    this.newPost.contenu = model.contenu;
    this.postsService.posts.push(this.newPost);
    this.options.resetModel();
    this.message = "Votre post à bien été ajouté!";
    setTimeout(() => {
      this.message = '';
      this.router.navigate(['posts']);
    }, 2000);
  }
}
